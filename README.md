# Administratifs du 24ème

Activité administrative de la société 24ème

## Mise à disposition des documents

- [Statuts de la société](https://github.com/24eme/administratif/blob/master/statuts_24eme.md)
- [Assemblée générale ordinaire annuelle du 30 mars 2016](https://github.com/24eme/administratif/blob/master/20160330_assemblee_generale_ordinaire.md)
- [Assemblée générale ordinaire annuelle du 23 novembre 2016](https://github.com/24eme/administratif/blob/master/20161123_assemblee_generale_ordinaire.md)
- [Assemblée générale extraordinaire du 9 janvier 2017](https://github.com/24eme/administratif/blob/master/20170109_assemblee_generale_extraordinaire.md)
- [Assemblée générale ordinaire annuelle du 30 mars 2017](https://github.com/24eme/administratif/blob/master/20170330_assemblee_generale_ordinaire.md)
- [Assemblée générale ordinaire annuelle du 23 novembre 2017](https://github.com/24eme/administratif/blob/master/20171123_assemblee_generale_ordinaire.md)
- [Assemblée générale ordinaire réunie à titre extraordinaire du 07 mai 2018](https://github.com/24eme/administratif/blob/master/20180507_assemblee_generale_ordinaire_extraordinaire.md)
- [Assemblée générale ordinaire annuelle du 30 mars 2018](https://github.com/24eme/administratif/blob/master/20180330_assemblee_generale_ordinaire.md)
- [Assemblée générale ordinaire annuelle du 23 novembre 2018](https://github.com/24eme/administratif/blob/master/20181123_assemblee_generale_ordinaire.md)
- [Assemblée générale extraordinaire du 14 mars 2019](https://github.com/24eme/administratif/blob/master/20190314_assemblee_generale_extraordinaire.md)

## Les comptes annuels

- [Tableaux des charges et produits](bilans.csv)

## Procédures administratives

- [Changement d'adresse](https://github.com/24eme/administratif/blob/master/docs/changement_adresse.md)
- [Publication des comptes](https://github.com/24eme/administratif/blob/master/docs/publication_comptes.md)
- [Procédure de dégrèvement à la CFE](https://github.com/24eme/administratif/blob/master/docs/degrevement_cfe.md)
- [Taxe d'apprentissage et formation continue](docs/taxe_apprentissage_et_formation_continue.md)

## Modèles de contrats

- [Conditions Générales](20171204_contrats_ConditionsGenerales.tmd)
- [Conditions Particulières Hébergement](20171204_contrats_ConditionsParticulieresHebergement.tmd)
- [Conditions Particulières Logiciel](20171204_contrats_ConditionsParticulieresLogiciel.tmd)
- [Conditions Particulières TMA](20171204_contrats_ConditionsParticulieresTMA.tmd)
- [Contrat de travail à Durée Indéterminée](contrat_travail_cdi.md)

## Utilisation

### Génération des PDF de documents à partir du markdown

Prérequis

    aptitude install pandoc pdflatex

Générer les pdf à partir du markdown

    make
