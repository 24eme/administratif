# Taxe d'apprentissage et formation continue

Ces taxes sont à payer en début d'année pour l'année en cours.

Elles sont toutes indexés sur la masse salariale.

Le cabinet comptable nous envoi les documents déjà complétés en février, il y a juste à les signer, joindre le paiement et les envoyer.

Tableau de la répartion des taxes par type et organisme :

| Type 	| Organisme 	| Année 	| Taux  	| Masse salariale brute (n-1)	| Montant HT 	|
|----------------------	|-----------	|-------	|---------	|-----------------	|------------	|
| Taxe d'apprentissage 	| CCI Paris 	| 2019 	| 0,68 % 	| 116 514 € 	| 792 € 	|
| Formation continue 	| FAFIEC 	| 2019 	| 0,575 % 	| 116 514 € 	| 670 € 	|
| Formation continue 	| ADESATT 	| 2019 	| 0,02 % 	| 116 514 € 	| 23 € 	|
| Taxe d'apprentissage 	| CCI Paris 	| 2018 	| 0,68 % 	| 140 874 € 	| 958 € 	|
| Formation continue 	| FAFIEC 	| 2018 	| 0,575 % 	| 140 874 € 	| 810 € 	|
| Formation continue 	| ADESATT 	| 2018 	| 0,02 % 	| 140 874 € 	| 28 € 	|
| Taxe d'apprentissage 	| CCI Paris 	| 2017 	| 0,68 % 	| 178 559 € 	| 1214 € 	|
| Formation continue 	| FAFIEC 	| 2017 	| 0,575 % 	| 178 559 € 	| 1027 € 	|
| Formation continue 	| ADESATT 	| 2017 	| 0,02 % 	| 178 559 € 	| 36 € 	|
